package models.mirela;

import static models.mirela.FTC.*;

class Train extends Thread {
    int state =  AWAY;

    @Override
    public void run() {
        state = WAIT;
        synchronized(controller) {
            state = TUNNEL;
        }
        state = AWAY;
    }
}
class TrainFaulty extends Thread {
    int state = AWAY;

    @Override
    public void run() {
        state = WAIT;
        state = TUNNEL;
        state = AWAY;
    }
}
public class FTC {
    final static int AWAY = 0;
    final static int WAIT = 1;
    final static int TUNNEL = 2;
    final static Object controller = new Object();
    
    public static void main(String[] args) {
        (new Train()).start();
        (new Train()).start();
        (new TrainFaulty()).start();
    }
}
