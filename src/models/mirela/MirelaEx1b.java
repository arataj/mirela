package models.mirela;

import mirela.*;

public class MirelaEx1b {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic s1 = new Periodic(m,
            new Delay(60, 80), new Delay(80, 100));
        Periodic s2 = new Periodic(m,
            new Delay(200, 300), new Delay(360, 400));
        Periodic s3 = new Periodic(m,
            new Delay(200, 300), new Delay(360, 400));
        Delay[] f1Delay = {
            new Delay(60, 80), new Delay(60, 80),
        };
        First f1 = new First(m, f1Delay);
        Delay[] f2Delay = {
            new Delay(80, 100), new Delay(80, 100),
        };
        First f2 = new First(m, f2Delay);
        s1.addOut(f1);
        s2.addOut(f2);
        s2.addOut(f1);
        s3.addOut(f2);
        Both b = new Both(m, new Delay(20, 60));
        s3.addOut(b);
        f2.addOut(b);
        Delay[] mDelay = {
            new Delay(20, 60), new Delay(20, 60),
        };
        Memory m1 = new Memory(m, mDelay);
        f1.addOut(m1);
        b.addOut(m1);
        Rendering r = new Rendering(m, m1,
                new Delay(20, 60), new Delay(80, 100));
        m.enable();
    }
}
