package models.mirela;

import mirela.*;

public class MirelaEx3 {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic s1 = new Periodic(m,
            new Delay(50, 75), new Delay(75, 100));
        Periodic s2 = new Periodic(m,
            new Delay(200, 300), new Delay(350, 400));
        Periodic s3 = new Periodic(m,
            new Delay(200, 300), new Delay(350, 400));
        Delay[] f1Delay = {
            new Delay(50, 75), new Delay(50, 75),
        };
        First f1 = new First(m, f1Delay);
        Delay[] f2Delay = {
            new Delay(75, 100), new Delay(75, 100),
        };
        First f2 = new First(m, f2Delay);
        s1.addOut(f1);
        s2.addOut(f2);
        s2.addOut(f1);
        s3.addOut(f2);
        Both b = new Both(m, new Delay(25, 50));
        s3.addOut(b);
        f2.addOut(b);
        Delay[] mDelay = {
            new Delay(25, 50), new Delay(25, 50),
        };
        Memory m1 = new Memory(m, mDelay);
        f1.addOut(m1);
        b.addOut(m1);
        Rendering r = new Rendering(m, m1,
                new Delay(25, 50), new Delay(50, 75));
        m.enable();
    }
}
