package models.mirela;

import mirela.*;

public class MirelaR {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic c = new Periodic(m,
            new Delay(2000, 3000), new Delay(3500, 4500));
        Aperiodic o = new Aperiodic(m, 50);
        Periodic p = new Periodic(m,
            new Delay(2600, 3600), new Delay(2600, 3600));
        Priority pr = new Priority(m, new Delay(2000, 3000), new Delay(2000, 3000));
        c.addOut(pr);
        o.addOut(pr);
        Delay[] fDelay = {
            new Delay(50, 75), new Delay(50, 75),
        };
        First f = new First(m, fDelay);
        o.addOut(f);
        p.addOut(f);
        Delay[] m1Delay = {
            new Delay(200, 300),
        };
        Memory m1 = new Memory(m, m1Delay);
        pr.addOut(m1);
        Delay[] m2Delay = {
            new Delay(20, 30),
        };
        Memory m2 = new Memory(m, m2Delay);
        f.addOut(m2);
        Delay[] m3Delay = {
            new Delay(10, 20),
        };
        Memory m3 = new Memory(m, m3Delay);
        f.addOut(m3);
        Rendering g = new Rendering(m, m1,
            new Delay(210, 310), new Delay(400, 500));
        Rendering h = new Rendering(m, m2,
            new Delay(20, 30), new Delay(10, 20));
        Rendering i = new Rendering(m, m3,
            new Delay(10, 20), new Delay(10, 20));
        m.enable();
    }
}
