package models.mirela;

import mirela.*;

public class MirelaS {
    public static void main(String[] args) {
        final int MIN = 0;
        final int MAX = 5000;
        Delay d = new Delay(MIN, MAX);
        Mirela m = new Mirela();
        Periodic c = new Periodic(m, d, d);
        Aperiodic o = new Aperiodic(m, MAX);
        Periodic p = new Periodic(m, d, d);
        Priority pr = new Priority(m, d, d);
        c.addOut(pr);
        o.addOut(pr);
        Delay[] fDelay = {
            d, d,
        };
        First f = new First(m, fDelay);
        o.addOut(f);
        p.addOut(f);
        Delay[] m1Delay = {
            d,
        };
        Memory m1 = new Memory(m, m1Delay);
        pr.addOut(m1);
        Delay[] m2Delay = {
            d,
        };
        Memory m2 = new Memory(m, m2Delay);
        f.addOut(m2);
        Delay[] m3Delay = {
            d,
        };
        Memory m3 = new Memory(m, m3Delay);
        f.addOut(m3);
        Rendering g = new Rendering(m, m1,
            d, d);
        Rendering h = new Rendering(m, m2,
            d, d);
        Rendering i = new Rendering(m, m3,
            d, d);
        m.enable();
    }
}
