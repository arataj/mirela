package models.mirela;

import mirela.*;

public class MirelaT {
    public static void main(String[] args) {
        final int MIN = 1000;
        final int MAX = 5000;
        Delay dZ = new Delay(0, MAX);
        Delay d = new Delay(MIN, MAX);
        Mirela m = new Mirela();
        Periodic c = new Periodic(m, d, d);
        Aperiodic o = new Aperiodic(m, MIN);
        Periodic p = new Periodic(m, d, d);
        Priority pr = new Priority(m, d, d);
        c.addOut(pr);
        o.addOut(pr);
        Delay[] fDelay = {
            dZ, dZ,
        };
        First f = new First(m, fDelay);
        o.addOut(f);
        p.addOut(f);
        Delay[] m1Delay = {
            dZ,
        };
        Memory m1 = new Memory(m, m1Delay);
        pr.addOut(m1);
        Delay[] m2Delay = {
            dZ,
        };
        Memory m2 = new Memory(m, m2Delay);
        f.addOut(m2);
        Delay[] m3Delay = {
            dZ,
        };
        Memory m3 = new Memory(m, m3Delay);
        f.addOut(m3);
        Rendering g = new Rendering(m, m1,
            dZ, dZ);
        Rendering h = new Rendering(m, m2,
            dZ, dZ);
        Rendering i = new Rendering(m, m3,
            dZ, dZ);
        m.enable();
    }
}
