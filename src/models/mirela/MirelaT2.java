package models.mirela;

import mirela.*;

public class MirelaT2 {
    public static void main(String[] args) {
        final int MIN = 1000;
        final int MAX = 5000;
        Delay dZ = new Delay(0, MAX);
        Delay d = new Delay(MIN, MAX);
        Mirela m = new Mirela();
        Periodic c = new Periodic(m, d, d);
        Aperiodic o = new Aperiodic(m, MIN);
        Periodic p = new Periodic(m, d, d);
        Priority pr = new Priority(m, d, d);
        c.addOut(pr);
        o.addOut(pr);
        Delay[] fDelay = {
            dZ, dZ,
        };
        First f = new First(m, fDelay);
        o.addOut(f);
        p.addOut(f);
        m.enable();
    }
}
