package models.mirela;

import mirela.*;

public class PC {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic camera = new Periodic(m,
            new Delay(0, 100), new Delay(100, 100));
        Delay[] busDelay = {
            new Delay(20, 20),
        };
        First bus = new First(m, busDelay);
        camera.addOut(bus);
        Rendering rendering = new Rendering(m, m1,
                new Delay(25, 50), new Delay(75, 100));
        m.enable();
    }
}
