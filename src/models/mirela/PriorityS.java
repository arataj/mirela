/*
 * PriorityS.java
 *
 * Created on Mar 25, 2015
 *
 * Copyright (c) 2015  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package models.mirela;

import mirela.*;

public class PriorityS extends Priority {
    public PriorityS(Mirela system, AbstractDelay master, AbstractDelay both) {
        super(system, master, both);
    }
    @Override
    public void run() {
        // sync with slave
        sync.consume(producers[1].INDEX);
        // sync with the first consumer
        consumers[0].put(INDEX);
        super.run();
    }
}
