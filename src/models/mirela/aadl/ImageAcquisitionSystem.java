/*
 * ImageAcquisitionSystem.java
 *
 * Created on Mar 26, 2018
 *
 * Copyright (c) 2018  Artur Rataj.
 *
 * This code is distributed under the terms of the GNU Library
 * General Public License, either version 3 of the license or, at
 * your option, any later version.
 */

package models.mirela.aadl;

import java.util.*;
import mirela.*;

/**
 * An image acquisition system. There is a camera, a bus and a processor.
 * 
 * @author Artur Rataj
 */
public class ImageAcquisitionSystem {
    // time period of producing frames in ms
    final static int FRAME_PERIOD = 15;
    // size of a single frame
    final static long FRAME_SIZE = 300*1000;
    // a fraction of the camera period due to low exposure
    final static int CAMERA_ACQ_TIME = 2;
    // an upper limit, due to a variable frame complexity
    final static int CAMERA_SEND_MAX_TIME = 2;
    // a fixed packet latency
    final static double ETH_TRANSMISSION_TIME_FIXED = 2;
    // an upper limit, due to a variable frame complexity
    final static double ETH_TRANSMISSION_TIME_PER_BYTE_MAX = 1.0/FRAME_SIZE;
    // phase difference between the camera and the video
    // processing, must take into account the transmission
    // times
    final static int VIDEO_PROCESSING_PHASE = 2;
    // an upper limit, due to a variable frame complexity
    final static int VIDEO_PROCESSING_MAX_TIME = 4;
    
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Camera cam = new Camera(m, new Delay(0), FRAME_PERIOD,
                CAMERA_ACQ_TIME, CAMERA_SEND_MAX_TIME);
        Bus eth = new Bus(m,
                new Delay(ETH_TRANSMISSION_TIME_FIXED),
                new Delay(0, ETH_TRANSMISSION_TIME_PER_BYTE_MAX));
        cam.addOut(eth, FRAME_SIZE);
        VideoProcessing vp = new VideoProcessing(m,
                new Delay(VIDEO_PROCESSING_PHASE),
                FRAME_PERIOD, VIDEO_PROCESSING_MAX_TIME);
        eth.addOut(vp);
        m.enable();
    }
}
