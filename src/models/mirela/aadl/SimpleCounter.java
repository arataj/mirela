package models.mirela.aadl;

import mirela.*;
import mirela.aadl.*;

public class SimpleCounter {
    public static void main(String[] args) {
        Mirela m = new Mirela();
        Periodic p = new Periodic(m,
            new Delay(40, 60), new Delay(90, 100));
        int[] steps = { 1 };
        Counter c = new Counter(m, 4, steps);
        p.addOut(c);
        m.enable();
    }
}
 
