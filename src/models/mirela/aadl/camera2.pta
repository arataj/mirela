pta

const int camera_period           = 15;
const int camera_acq_time         = 2;
const int camera_send_time_max    = 2;
const int bus_time                = 3;
const int processor_period        = 15;
const int processor_read_time_max = 1;
const int processor_calc_time_max = 4;

const int phase = 8;

module Camera_pt

    xct : clock;

    invariant
        (true => xct<=camera_period)
    endinvariant

    [ctick] xct>=camera_period -> (xct'=0);

endmodule

module Camera

    sc : [0..3] init 0;
    xc : clock;

    invariant
        (sc=1 => xc<=camera_acq_time) &
        (sc=2 => xc<=camera_send_time_max) &
        (sc=3 => xc<=0)
    endinvariant

    [ctick]     sc=0                       -> (sc'=1)&(xc'=0);
    []          sc=1 & xc>=camera_acq_time -> (sc'=2)&(xc'=0);
    []          sc=2                       -> (sc'=3)&(xc'=0);
    [new_frame] sc=3                       -> (sc'=0)&(xc'=0);

endmodule

module Bus

    sb : [0..2] init 0;
    xb : clock;

    invariant
        (sb=1 => xb<=bus_time)
    endinvariant

    [new_frame] sb=0                -> (sb'=1)&(xb'=0);
    []          sb=1 & xb>=bus_time -> (sb'=2);
    [frame_tr]  sb=2                -> (sb'=0);

endmodule

module Processor_pt

    spt : [0..1] init 0;
    xpt : clock;

    invariant
        (spt=0 => xpt<=phase) &
        (spt=1 => xpt<=processor_period)
    endinvariant

    []      spt=0 & xpt>=phase            -> (spt'=1)&(xpt'=0);
    [ptick] spt=1 & xpt>=processor_period -> (xpt'=0);

endmodule

module Processor

    sp : [0..3] init 0;
    xp : clock;

    invariant
        (sp=1 => xp<=processor_read_time_max) &
        (sp=2 => xp<=processor_calc_time_max)
    endinvariant

    [ptick]     sp=0 -> (sp'=1)&(xp'=0);
    [frame_tr]  sp=1 -> (sp'=2)&(xp'=0);
    [calc_done] sp=2 -> (sp'=0);
    
endmodule

module Measure

    sm : [0..1] init 0;
    xm : clock;

    [ctick]     true -> (sm'=0)&(xm'=0);
    [calc_done] true -> (sm'=1);

endmodule