package pmgcs;

import java.util.Random;

import hc.*;
import hc.game.*;

public class Dialogue5s {
    final public static int NUM_SOURCES = 2;
    final public static int NUM_TURNS = 4;
    final public static int BASE = 100;
    final public static Source[] SOURCES = new Source[NUM_SOURCES];
    final public static int COMP_A = 0;
    final public static int COMP_B = 1;
    final public static int CONJ_ESPECIALLY = 0;
    final public static int CONJ_DESPITE    = 1;
    final public static int LEVELS = 3;
    final private static int CHEATING_INT = 1;
    final public static boolean CHEATING_POSSIBLE = CHEATING_INT == 1;
    /* -1 for variable declaration */
    final public static int FIX_DECLARATION = -1;
    
    public static Listener LISTENER;
    public static int /*@(0, Dialogue5.NUM_TURNS + 1)*/ turnCount = 0;
    static int[] A_LIST = { fix(80), fix(80) };
    static int[] B_LIST = { fix(80), fix(80) };
    
    // only for positive values
    public static int fix(double value) {
        return (int)(value*BASE/100.0 + 0.5);
    }
    public static boolean isEnd() {
        ++turnCount;
        return turnCount == Dialogue5s.NUM_TURNS + 1;
    }
    public static void main(String[] args) {
        System.out.println("turns = " + NUM_TURNS +
                "\tfix declar = " + FIX_DECLARATION +
                "\tcheat possible = " + CHEATING_POSSIBLE);
        TurnGame table = new TurnGame();
        Model.name(table);
        // create sources
        for(int i = 0; i < NUM_SOURCES; ++i) {
            Source c = new Source(table, i, A_LIST[i], B_LIST[i]);
            c.start();
            Model.name(c, "", "" + (i + 1));
            Model.player(c, "s" + (i + 1));
            SOURCES[i] = c;
        }
        LISTENER = new Listener(table, NUM_SOURCES,
            COMP_A, CONJ_DESPITE, COMP_B);
        LISTENER.start();
        Model.name(LISTENER, "", "");
        Model.player(LISTENER, "l");
        table.start(NUM_SOURCES + 1);
        // Model.check("game ends",
        //     "<<2>> Pmin=? [F turnCount=3]");
        Model.check("persuasion min",
            "<<2>> Pmin=? [F persuaded=1]");
        Model.check("persuasion max",
            "<<2>> Pmax=? [F persuaded=1]");
        Model.waitFinish();
        System.out.println("finished");
        // intterupt any thread still running
        SOURCES[0].interrupt();
    }
}
class Source extends TurnPlayer {
    protected final Random strategy;
    public final int A;
    public final int B;
    // sensors always give some information (to avoid division by zero in the listener)
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue5.LEVELS - 1)*/ declareA =
        (getNum() == 0 || Dialogue5s.FIX_DECLARATION == -1 ? Dialogue5s.LEVELS - 1 : Dialogue5s.FIX_DECLARATION);
    public int/*@(getNum() == 0 ? 1 : 0, Dialogue5.LEVELS - 1)*/ declareB =
        (getNum() == 0 || Dialogue5s.FIX_DECLARATION == -1 ? Dialogue5s.LEVELS - 1 : Dialogue5s.FIX_DECLARATION);
    public int outA;
    public int outB;
    
    public Source(TurnGame table, int playerNum,
            int a, int b) {
        super(table, playerNum);
        strategy = new Random();
        A = a;
        B = b;
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(isInterrupted())
                break;
            say();
            turnNext(getNum() + 1);
        }
    }
    protected void say() {
        if(getNum() == 0) {
        /*
            // let it be allowed to replace an intermediate value
            declareA = strategy.nextInt(Dialogue5.LEVELS - 1);
            declareA = 1 + declareA;
            // let it be allowed to replace an intermediate value
            declareB = strategy.nextInt(Dialogue5.LEVELS - 1);
            declareB = 1 + declareB;
         */
            // sensors give exact values
            declareA = Dialogue5s.LEVELS - 1;
            declareB = Dialogue5s.LEVELS - 1;
            outA = 80;
            outB = 80;
        } else {
            // choose declaration strength
          if(Dialogue5s.FIX_DECLARATION == -1) {
              declareA = strategy.nextInt(Dialogue5s.LEVELS);
              declareB = strategy.nextInt(Dialogue5s.LEVELS);
              switch(Dialogue5s.turnCount) {
                  case 0:
                      declareA = 0;
                      declareB = 1;
                      outA = 1000;
                      outB = 0;
                      break;
                  case 1:
                      declareA = 0;
                      declareB = 2;
                      outA = 1000;
                      outB = 80;
                      break;
                  case 2:
                      declareA = 0;
                      declareB = 2;
                      outA = 1000;
                      outB = 80;
                      break;
                  case 3:
                      declareA = 0;
                      declareB = 2;
                      outA =  1000;
                      outB = -80;
                      break;
              }
          } else {
              declareA = Dialogue5s.FIX_DECLARATION;
              declareB = Dialogue5s.FIX_DECLARATION;
          }
    //        declareA = 0;
    //        declareB = 0;
        }
        // choose if to cheat
        //outA = getOutValue(A);
        //outB = getOutValue(B);
    }
    protected int getOutValue(int in) {
        if(!Dialogue5s.CHEATING_POSSIBLE || getNum() == 0)
            return in;
        else {
            switch(strategy.nextInt(3)) {
                case 0:
                    return -in;
                case 1:
                    return in;
                case 2:
                    return 0;
            }
            /*
            switch(strategy.nextInt(5)) {
                case 0:
                    return -in;
                case 1:
                    return -in/2;
                case 2:
                    return 0;
                case 3:
                    return in/2;
                case 4:
                    return in;
            }
             */
            return 19;
        }
    }
}
class Listener extends TurnPlayer {
    protected final Random strategy;
    final int COMP_1;
    final int CONJ;
    final int COMP_2;
    int[]/*@(0, Dialogue5.BASE)*/ compliance = {Dialogue5s.fix(90), Dialogue5s.fix(50)};
    int[]/*@(0, Dialogue5.BASE)*/ defiance = {0, Dialogue5s.fix(10)};
    double yes = 0;
    double persuaded = 0;
    
    public Listener(TurnGame table, int playerNum,
            int comp1, int conj, int comp2) {
        super(table, playerNum);
        strategy = new Random();
        COMP_1 = comp1;
        CONJ = conj;
        COMP_2 = comp2;
        // Model.name(compliance, "", "");
    }
    @Override
    public void run() {
        while(true) {
            turnWait();
            if(Dialogue5s.isEnd()) {
                break;
            }
            decide();
            turnNext(0);
        }
        persuaded = yes/Dialogue5s.NUM_TURNS;
        System.out.println("persuaded=" + persuaded);
        Model.finish();
    }
    protected int getWeightedSignal(int source, int declare, int out) {
        Source s = Dialogue5s.SOURCES[source];
        return (compliance[source] + defiance[source])*declare*
                (compliance[source] - defiance[source])*out;
    }
    protected int getWeight(int source, int declare) {
        Source s = Dialogue5s.SOURCES[source];
        return (compliance[source] + defiance[source])*declare*
                Dialogue5s.BASE;
    }
    protected void decide() {
        final int MAX_WEIGHT = Dialogue5s.BASE*Dialogue5s.NUM_SOURCES*(Dialogue5s.LEVELS - 1);
        int/*@(0, MAX_WEIGHT*Dialogue5.BASE*Dialogue5.BASE)*/ sSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue5.BASE)*/ wSumA = 0;
        int/*@(0, MAX_WEIGHT*Dialogue5.BASE*Dialogue5.BASE)*/ sSumB = 0;
        int/*@(0, MAX_WEIGHT*Dialogue5.BASE)*/ wSumB = 0;
//        Dialogue5.SOURCES[1].outA = -80;
//        Dialogue5.SOURCES[1].outB = 80;
//        Dialogue5.SOURCES[1].declareA = 2;
//        Dialogue5.SOURCES[1].declareB = 0;
        Source s = Dialogue5s.SOURCES[0];
        int sumB1 = getWeightedSignal(0, s.declareB, s.outB);;
        int weightB1 = getWeight(0, s.declareB);
        s = Dialogue5s.SOURCES[1];
        int sumB2 = getWeightedSignal(1, s.declareB, s.outB);
        int weightB2 = getWeight(1, s.declareB);
        for(int i = 0; i < Dialogue5s.NUM_SOURCES; ++i) {
            s = Dialogue5s.SOURCES[i];
            sSumA = sSumA + getWeightedSignal(i, s.declareA, s.outA);
            wSumA = wSumA + getWeight(i, s.declareA);
            sSumB = sSumB + getWeightedSignal(i, s.declareB, s.outB);
            wSumB = wSumB + getWeight(i, s.declareB);
        }
        int/*@(0, Dialogue5.BASE)*/ a;
        int/*@(0, Dialogue5.BASE)*/ b;
        int b1 = sumB1/weightB1;
        int b2 = sumB2/weightB2;
        a = sSumA/wSumA;
        b = sSumB/wSumB;
        int primary;
        switch(COMP_1) {
            case Dialogue5s.COMP_A:
                primary = a;
                break;
                
            case Dialogue5s.COMP_B:
                primary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component A");
        }
        int secondary;
        switch(COMP_2) {
            case Dialogue5s.COMP_A:
                secondary = a;
                break;
                
            case Dialogue5s.COMP_B:
                secondary = b;
                break;
                
            default:
                throw new RuntimeException("unknown component B");
        }
        double p = 0.4 + 0.2*primary/Dialogue5s.BASE +
                (CONJ == Dialogue5s.CONJ_ESPECIALLY ? 0.1 : -0.1)*
                    (secondary - Dialogue5s.BASE/2)/(Dialogue5s.BASE/2);
        System.out.print(primary + " " + secondary + "\tt=" + Dialogue5s.turnCount + " p=" + p);
        yes += p;
        // decrease trust, if cheating discovered
        modifyAttitude(0);
        modifyAttitude(1);
        System.out.println("\tc/d " + compliance[1] + " " + defiance[1]);
        //
        // cull state space
        //
        Dialogue5s.SOURCES[1].outA = -Dialogue5s.fix(80);
        Dialogue5s.SOURCES[1].outB = Dialogue5s.fix(80);
        Dialogue5s.SOURCES[1].declareA = 1;
        Dialogue5s.SOURCES[1].declareB = 1;
    }
    protected void modifyAttitude(int comp) {
        int out0;
        int out1;
        int declare;
        switch(comp) {
            case 0:
                out0 = Dialogue5s.SOURCES[0].outA;
                out1 = Dialogue5s.SOURCES[1].outA;
                declare = Dialogue5s.SOURCES[1].declareA;
                break;
                
            case 1:
                out0 = Dialogue5s.SOURCES[0].outB;
                out1 = Dialogue5s.SOURCES[1].outB;
                declare = Dialogue5s.SOURCES[1].declareB;
                break;
                
            default:
                throw new RuntimeException("unknown component");
        }
        if(out0 != out1) {
            if(true) {
                if(declare > 0) {
                    compliance[1] = compliance[1] -
                            declare*declare*(out1 - out0)*(out1 - out0)*(Dialogue5s.BASE/10)
                                /Dialogue5s.BASE/Dialogue5s.BASE;
                    if(compliance[1] < 0)
                        compliance[1] = 0;
                }
            } else {
                if(declare > 0) {
                    defiance[1] = defiance[1] +
                            declare*declare*(out1 - out0)*(out1 - out0)*(Dialogue5s.BASE/10)
                                /Dialogue5s.BASE/Dialogue5s.BASE;
                    if(defiance[1] > Dialogue5s.BASE)
                        defiance[1] = Dialogue5s.BASE;
                }
            }
        }
    }
}
